import string

class Aircraft:
    """
         Representación de un Avión

         Attributes:
            registration(str): matrícula del avión
            model(str): modelo del avión
            num_rows(int): número de filas del avión
            num_seats_per_row(int): número de sillas por fila

        Methods:
            seating_plan(): Retorna e imprime por pantalla el plan de asientos del avión
            num_seats(): Número total de asientos del avión
    """

    def __init__(self, registration, model, num_rows, num_seats_per_row):
        self.__registration = registration
        self.__model = model
        self.__num_rows = num_rows
        self.__num_seats_per_row = num_seats_per_row


    def get_registration(self):
        return self.__registration

    def get_model(self):
        return self.__model


    def seating_plan(self):
        """ Devuelve el plan de asientos de la aeronave, listando el numero de asientos
        y mostrando los asientos por filas como letras 

        Returns:
            (lista, string): tupla que contiene una lista con el numero de asientos de la aereonave, y un string con los asientos por cada fila 
        """
        
        # Generamos las letras en funcion de los posibles asientos por fila
        seats = ""
        numeros_alfabeto = dict(zip(range(1, 27), string.ascii_uppercase))
        for i in range(0, self.__num_seats_per_row):
            seats = seats + str(numeros_alfabeto[i+1])

        rows = []
        for r in range(0, self.__num_rows):
            rows.append(r+1)

        return rows, seats

    
    def num_seats(self):
        """Devuelve el numero total de asientos de la aereonave
        Returns:
            int: numero de asientos
        """
        return (self.__num_rows * self.__num_seats_per_row)
    


class Airbus(Aircraft):

    def __init__(self, registration, variant):
        super().__init__(registration, "Airbus A319", 23, 6)
        self.__variant = variant


    def get_variant(self):
        return self.__variant



class Boeing(Aircraft):

    def __init__(self, registration, airline):
        super().__init__(registration, "Boeing 777", 56, 9)
        self.__airline = airline


    def get_airline(self):
        return self.__airline