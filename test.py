import unittest
from aircraft import *
from flight import *
from passenger import *

class Test(unittest.TestCase):

    def test_flight_createion(self):
        test_aircraft = Aircraft(registration = "G-EUAH", model = "Airbus A319", num_rows = 22, num_seats_per_row=6)
        test_flight_num = "B1117"

        self.assertRaisesRegex(
            ValueError, 
            "El valor del vuelo B1117 es incorrecto, caracteres iniciales no son letras", 
            Flight, test_flight_num, test_aircraft)
        
        test_flight_num = "Ba117"
        self.assertRaisesRegex(
            ValueError, 
            "El valor del vuelo Ba117 es incorrecto, caracteres inciales no son mayusculas", 
            Flight, test_flight_num, test_aircraft)

        test_flight_num = "BAA17"
        self.assertRaisesRegex(
            ValueError, 
            "El valor del vuelo BAA17 es incorrecto, la numeración del vuelo es incorrecta", 
            Flight, test_flight_num, test_aircraft)

        test_flight_num = "BA117"
        self.assertIsNotNone(Flight(test_flight_num, test_aircraft))
        
        self.assertEqual(Flight(test_flight_num, test_aircraft).get_number(), test_flight_num)
        self.assertEqual(Flight(test_flight_num, test_aircraft).get_aircraft().get_registration(), "G-EUAH")
        self.assertEqual(Flight(test_flight_num, test_aircraft).get_aircraft().get_model(), "Airbus A319")
        self.assertEqual(Flight(test_flight_num, test_aircraft).get_aircraft().num_seats(), 22 * 6)

        #test_flight_num = None
        #self.assertEqual(Flight(test_flight_num, test_aircraft).get_number(), None)

    def test_passenger(self):
        test_aircraft = Aircraft(registration = "G-EUAH", model = "Airbus A319", num_rows = 22, num_seats_per_row=6)
        available_seats = 22*6
        test_flight_num = "BA117"
        f1 = Flight(test_flight_num, test_aircraft)
        

        self.assertEqual(f1.num_available_seats(), available_seats)

        # Asigna un pasajero
        self.assertRaisesRegex(
            ValueError, 
            "Error en el plan de asientos del vuelo BA117 no hay letra especificada para el asiento", 
            f1.allocate_passenger, "123", ("Jack", "Shephard", "85994003S"))

        self.assertRaisesRegex(
            ValueError, 
            "Error en el plan de asientos del vuelo BA117 formato de la numeración de los asientos incorrecta", 
            f1.allocate_passenger, "1AA", ("Jack", "Shephard", "85994003S"))

        self.assertRaisesRegex(
            ValueError, 
            "Error en el plan de asientos del vuelo BA117 no existen asientos Z para el avión designado", 
            f1.allocate_passenger, "12Z", ("Jack", "Shephard", "85994003S"))
        
        self.assertRaisesRegex(
            ValueError, 
            "Error en el plan de asientos del vuelo BA117 no existe la fila 30 para el avión designado", 
            f1.allocate_passenger, "30A", ("Jack", "Shephard", "85994003S"))
        
        f1.allocate_passenger("12A", ("Jack", "Shephard", "85994003S"))

        # Test para un pasajero asignado al vuelo
        self.assertNotEqual(str(f1.print_seating()).find("Jack"), -1)
        self.assertNotEqual(str(f1.print_seating()).find("Shephard"), -1)
        self.assertNotEqual(str(f1.print_seating()).find("85994003S"), -1)
        self.assertEqual(f1.num_available_seats(), (available_seats-1))

        
        

        # Test para un pasajero reasignado, 
        # comprueba que este sigue estando en el vuelo, 
        # y que el numero de asientos disponible sigue correcto

        self.assertRaisesRegex(
            ValueError, 
            "El asiento 16A no está ocupado", 
            f1.reallocate_passenger, "16A", "15F")
        
        f1.reallocate_passenger("12A", "15F")
        
        
        self.assertNotEqual(str(f1.print_seating()).find("Jack"), -1)
        self.assertNotEqual(str(f1.print_seating()).find("Shephard"), -1)
        self.assertNotEqual(str(f1.print_seating()).find("85994003S"), -1)

        self.assertEqual(f1.num_available_seats(), available_seats-1)


        # Se añaden más pasajeros para comprobar la disponibilidad de asientos
        f1.allocate_passenger("18F", ("Kate", "Austen", "12589756P"))
        f1.allocate_passenger("18E", ("James", "Ford", "56278665F"))
        f1.allocate_passenger("1C", ("John", "Locke", "10265448H"))
        f1.allocate_passenger("4D", ("Sayid", "Jarrah", "15758664M"))
        self.assertEqual(f1.num_available_seats(), available_seats-5)
    

if __name__ == "__main__":
    unittest.main()

