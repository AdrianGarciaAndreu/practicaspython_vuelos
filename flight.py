from pprint import pprint
import string

class Flight:
    """
        Clase que representa un vuelo

        Attributes:
            number(str): número del vuelo, identificador
            aircraft(Aircraft): Avión que realiza el vuelo
            seating(str): plan de asientos para el vuelo

        Methods:
            allocate_passenger(str, Passenger): Asigna un pasajero a un asiento del plan de asientos
            reallocate_passenger(str, str): Reasigna de un asiento a otro a un pasajero
            num_available_seats(): Obtiene el número de asientos disponibles actualmente
            print_seating(): Retorna e imprime por pantalla el plan de asientos del vuelo
            print_boarding_cards(): imprime por pantalla las tarjetas de embarque de los pasajeros del vuelo


    """

    def __init__(self, number, aircraft):
        self.__number = number
        self.__aircraft = aircraft
        self.__seating = []
        
        try:
            self.__check_flight_number(number) #Test de formato del numbero del vuelo

            seating_plan = aircraft.seating_plan()

            self.__seating.insert(0, None) # Posicion 0 vacía
            for row in seating_plan[0]: # Por cada fila
                seat = {key: None for (key) in seating_plan[1]}
                self.__seating.insert(row, seat)
        
        except ValueError as err:
            raise err



    def get_number(self):
        return self.__number

    def get_aircraft(self):
        return self.__aircraft

    

    def allocate_passenger(self, seat, passenger):
        """ Asigna un pasajero a un asiento del plan de asientos
        Args:
        seat(str): Asiento al que designar al pasajero (Ej: 12C)
        passenger(Passenger): Pasajero al que asignar el asiento

        """  
        seat_row, seat_letter = self.__parse_seat(seat)

        if (self.__check_seat_occuped(seat_row, seat_letter)): 
            raise ValueError("El asiento "+str(str(seat_row)+str(seat_letter))+" está ocupado, fallo al asignar al pasajero "+str(passenger[0])+" "+str(passenger[1]))

        seat = {seat_letter: passenger}
        self.__seating[seat_row].update(seat)
        

        
    def reallocate_passenger(self, from_seat, to_seat):
        """ Reasigna de un asiento a otro a un pasajero

        Args:
        from_seat(str): Asiento asignado al pasajero
        to_seat(str): Asiento nuevo en el que asignar al pasajero
        """

        # Eliminar de original

        seat_row, seat_letter = self.__parse_seat(from_seat)
        if (self.__check_seat_occuped(seat_row, seat_letter)==False):  # Comprueba que en el asiento existe un pasajero
            raise ValueError("El asiento "+str(str(seat_row)+str(seat_letter))+" no está ocupado")

        passenger = self.__seating[seat_row].get(seat_letter)
        seat = {seat_letter: None}
        
        self.__seating[seat_row].update(seat)

        # Mover al asiento nuevo
        self.allocate_passenger(to_seat, passenger)



    def num_available_seats(self):
        """ Obtiene el número de asientos disponibles actualmente
        Returns:
            Número de asientos disponibles
        """

        available_seats = 0
        for i in range(1, (len(self.__seating))):
            for v in self.__seating[i].values():
                if(v == None): 
                    available_seats = available_seats+1

        return available_seats

    
    def print_seating(self):
        """ Retorna e imprime por pantalla el plan de asientos del vuelo

        Returns:
            Retorna el plan de asientos
        """

        seating = [row for row in self.__seating if row !=None]
        pprint(seating)

        return seating




    def print_boarding_cards(self):        
        """ Imprime por pantalla las tarjetas de embarque de los pasajeros del vuelo
        """

        passengers = self.__passenger_seats()
        p = next(passengers, None)
        while( p!=None):
            pprint("----------------------------------------------------") 
            pprint("|"+str(p[0])+" "+str(p[1])+" "+str(p[2])+" "+str(p[2])+" "+str(self.__number)+" "+str(self.__aircraft.get_model())+"|")
            pprint("----------------------------------------------------")           
            p = next(passengers, None)


    def __parse_seat(self, seat):
        """ Divide un asiento por su numero y letra
        Args:
        seat: Asiento el cual dividir su número y letra
        Returns:
        row: fila la que pertenece el asiento
        letter: letra del asiento
        """

        rows, seats = self.get_aircraft().seating_plan() # Filas y asientos del avion para comprobar que se traducen correctamente los asientos

        # Letra del asiento
        seat_letter = seat[-1]

        if(seat_letter.isalpha()):
            if(seat_letter not in seats): 
                raise ValueError("Error en el plan de asientos del vuelo "+self.__number+" no existen asientos "+str(seat_letter)+" para el avión designado")
        else: 
            raise ValueError("Error en el plan de asientos del vuelo "+self.__number+" no hay letra especificada para el asiento")


        # Fila del asiento
        seat_row = int(seat[0])
        not_exists_row = False # causa de la excepcion a lanzar, 0 por defecto

        if(len(seat)>2):
            try:
                seat_row = int(str(seat[0] + seat[1]))
                if (seat_row> max(rows)): 
                    not_exists_row = True
                    raise ValueError() # Lanza un error 
            except ValueError:
                if(not_exists_row):
                    raise ValueError("Error en el plan de asientos del vuelo "+self.__number+" no existe la fila "+str(seat_row)+" para el avión designado")
                else:
                    raise ValueError("Error en el plan de asientos del vuelo "+self.__number+" formato de la numeración de los asientos incorrecta")


        return seat_row, seat_letter



    def __passenger_seats(self):
        """ Generador para iterar sobre los asientos ocupados del vuelo
        Returns:
        generator: tupla con los datos del pasajero y su asiento
        """   

        indice = 1
        while(indice<len(self.__seating)):
            for k, v in self.__seating[indice].items():
                if(v != None):
                    yield v[0], v[1], v[2], str(str(indice)+k)
            indice = indice + 1


    def __check_flight_number(self, number):
        """ Comprueba el formato de la numeración de un vuelo, 
            en caso de ser incorrecto lanza una excepción de error de valores

        Args:
            number (str): número del vuelo a comprobar
        """
        flight_total_num = ""
        for char in number[0:2]:
            if(char.isalpha() == False):  raise ValueError("El valor del vuelo "+str(number)+" es incorrecto, caracteres iniciales no son letras")
            if(char.islower()): raise ValueError("El valor del vuelo "+str(number)+" es incorrecto, caracteres inciales no son mayusculas")
        for num in number[2:]:
            if(num.isnumeric() == False):  
                raise ValueError("El valor del vuelo "+str(number)+" es incorrecto, la numeración del vuelo es incorrecta")
            else: flight_total_num = flight_total_num + str(num)
        if (int(flight_total_num)>=9999):
            raise ValueError("El valor del vuelo "+str(number)+" es incorrecto, la numeración del vuelo supera la numeración máxima 9999")


    def __check_seat_occuped(self, seat_row, seat_letter):
        """ Comprueba si en un asiento determinado ya hay un pasajero,
            en caso de que esté ocupado, lanza una excepción
        Args:
            seat_row (int): fila del asiento a comprobar
            seat_letter (str): letra del asiento a comprobar
        Returns:
            Retorna si el asiento seleccionado está ocupado o no por otro pasajero
        """
        occuped = False
        passengers = self.__passenger_seats()
        p = next(passengers, None)
        while( p!=None):
            seat_row_occuped, seat_letter_occuped = self.__parse_seat(p[3])            
            if(seat_row == seat_row_occuped and seat_letter == seat_letter_occuped):
                occuped = True
            p = next(passengers, None)

        return occuped

