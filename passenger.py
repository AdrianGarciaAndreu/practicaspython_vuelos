class Passenger:
    """
        Clase que representa un pasajero

        Attributes:
            name(str): Nombre del pasajero 
            surname(str): Apellido del pasajero
            id_card(str): ID (DNI/NIE) del pasajero
            
        Methods:
            passenger_data(): Obtiene los datos de un pasajero
    """

    def __init__(self, name, surname, id_card):
        self.__name = name
        self.__surname = surname
        self.__id_card = id_card

    def passenger_data(self):
        """ Obtiene los datos de un pasajero
        Returns:
            (nombre, apellido, id): Devuelve una tupla con el nombre, apellido e id del pasajero
        """

        
        return self.__name, self.__surname, self.__id_card